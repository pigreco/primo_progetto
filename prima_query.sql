-- numero di segnalazioni per ogni mese 
SELECT T1.mese,nro_segnalazioni,Ta.nro_segnalazioni_aperta, nro_segnalazioni-Ta.nro_segnalazioni_aperta AS nro_segnalazioni_chiusa
FROM
(select DATE_PART("month", CAST(segnalazioniapprap.datetime AS DATETIME)) as mese, count(*) as nro_segnalazioni
from segnalazioniapprap
WHERE
segnalazioniapprap.latitude > 38.048841 and  segnalazioniapprap.latitude < 38.226443
and 
segnalazioniapprap.longitude > 13.240402 and segnalazioniapprap.longitude < 13.452891
group by mese) T1
LEFT JOIN
(select DATE_PART("month", CAST(segnalazioniapprap.datetime AS DATETIME)) as mese, count(*) as nro_segnalazioni_aperta
from segnalazioniapprap
WHERE
segnalazioniapprap.latitude > 38.048841 and  segnalazioniapprap.latitude < 38.226443
and 
segnalazioniapprap.longitude > 13.240402 and segnalazioniapprap.longitude < 13.452891
and
segnalazioniapprap.stato = 'aperta'
group by mese) Ta ON T1.mese = Ta.mese
ORDER BY T1.mese DESC